<?php $general_setting = DB::table('general_settings')->find(1); ?>
        <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{$general_setting->site_title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap/css/bootstrap.min.css') ?>" type="text/css">
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap/css/bootstrap-datepicker.min.css') ?>"
          type="text/css">
    <link rel="stylesheet" href="<?php echo asset('public/vendor/bootstrap/css/bootstrap-select.min.css') ?>"
          type="text/css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="<?php echo asset('public/vendor/font-awesome/css/font-awesome.min.css') ?>"
          type="text/css">
    <!-- Fontastic Custom icon font-->
    <link rel="stylesheet" href="<?php echo asset('public/css/fontastic.css') ?>" type="text/css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="<?php echo asset('public/css/grasp_mobile_progress_circle-1.0.0.min.css') ?>"
          type="text/css">
    <!-- Custom Scrollbar-->
    <link rel="stylesheet"
          href="<?php echo asset('public/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') ?>"
          type="text/css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="<?php echo asset('public/css/style.default.css') ?>" id="theme-stylesheet"
          type="text/css">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="<?php echo asset('public/css/custom-' . $general_setting->theme) ?>" type="text/css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <script type="text/javascript" src="<?php echo asset('public/vendor/jquery/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/vendor/jquery/jquery-ui.min.js') ?>"></script>
    <script type="text/javascript"
            src="<?php echo asset('public/vendor/jquery/bootstrap-datepicker.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/vendor/popper.js/umd/popper.min.js') ?>">
    </script>
    <script type="text/javascript" src="<?php echo asset('public/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript"
            src="<?php echo asset('public/vendor/bootstrap/js/bootstrap-select.min.js') ?>"></script>
    <script type="text/javascript"
            src="<?php echo asset('public/js/grasp_mobile_progress_circle-1.0.0.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/vendor/jquery.cookie/jquery.cookie.js') ?>">
    </script>
    <script type="text/javascript" src="<?php echo asset('public/vendor/chart.js/Chart.min.js') ?>"></script>
    <script type="text/javascript"
            src="<?php echo asset('public/vendor/jquery-validation/jquery.validate.min.js') ?>"></script>
    <script type="text/javascript"
            src="<?php echo asset('public/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js')?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/js/charts-home.js') ?>"></script>
    <script type="text/javascript" src="<?php echo asset('public/js/front.js') ?>"></script>
</head>
<body>
<div class="page login-page">
    <div class="main-content pt-5">
        <div class="header">
            <div class="container">
                <div class="header-body text-center">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8">
                            <img class="mb-5" src="https://tanfeed.ly/wp-content/uploads/2019/10/store.png"
                                 width="22%" alt="Tanfeed">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card mb-0 login-card-bg" style="background-color: #55588b">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-white mb-4">
                                <small>قم بتسجيل الدخول</small>
                            </div>

                            <div id="app"><!---->
                                <form method="POST" action="{{ route('login') }}" id="login-form"
                                      enctype="multipart/form-data"
                                      class="form-loading-button">
                                    @csrf
                                    <div class="form-group has-feedback">
                                        <div class="input-group input-group-merge input-group-alternative">
                                            @if (!$errors->has('name'))

                                                <input id="login-username" type="text" name="name" required
                                                       class="form-control" value=""
                                                >
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fa fa-envelope"></i></span>
                                                </div>
                                            @elseif ($errors->has('name'))

                                                <input id="login-username" type="text" name="name" required
                                                       class="form-control" value="">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text text-danger"><i
                                                                class="fa fa-envelope"></i></span>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <div class="input-group input-group-merge input-group-alternative">
                                            @if (!$errors->has('name'))

                                                <input id="login-password" type="password" name="password" required
                                                       class="form-control">
                                                <div class="input-group-prepend"><span class="input-group-text"><i
                                                                class="fa fa-unlock-alt"></i></span></div>
                                            @elseif ($errors->has('name'))

                                                <input id="login-password" type="password" name="password" required
                                                       class="form-control">
                                                <div class="input-group-prepend"><span
                                                            class="input-group-text text-danger"><i
                                                                class="fa fa-unlock-alt"></i></span></div>
                                            @endif
                                        </div> <!----></div>
                                    <div class="row align-items-center">
                                        <div class="col-xs-12 col-sm-8">
                                            <div class="custom-control custom-control-alternative custom-checkbox">
                                                <input id="checkbox-remember" name="remember" type="checkbox" value="1"
                                                       class="custom-control-input"> <label for="checkbox-remember"
                                                                                            class="custom-control-label"><span
                                                            class="text-white">ذكرني !</span></label></div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4">
                                            <button type="submit" data-loading-text="Loading..."
                                                    class="btn btn-success float-right header-button-top">
                                                <div class="aka-loader"></div>
                                                <span>دخول</span></button>
                                        </div>
                                    </div>
                                    <div class="mt-5 mb--4"><a href="{{url('register')}}"
                                                               class="text-white"><small>هل تمتلك حساب؟</small> سجل الان</a>
                                    </div>
                                </form>
                                <notifications></notifications>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer>
            <div class="container mt-5 mb-4">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-12">
                        <div class="copyright text-center text-white">
                            <small>
                                شركة تنفيذ لتقنية المعلومات © 2020
                            </small>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div>
</div>
</body>
</html>

<script type="text/javascript">
    $('.admin-btn').on('click', function () {
        $("input[name='name']").focus().val('admin');
        $("input[name='password']").focus().val('admin');
    });

    $('.staff-btn').on('click', function () {
        $("input[name='name']").focus().val('staff');
        $("input[name='password']").focus().val('staff');
    });
    // ------------------------------------------------------- //
    // Material Inputs
    // ------------------------------------------------------ //

    var materialInputs = $('input.input-material');

    // activate labels for prefilled values
    materialInputs.filter(function () {
        return $(this).val() !== "";
    }).siblings('.label-material').addClass('active');

    // move label on focus
    materialInputs.on('focus', function () {
        $(this).siblings('.label-material').addClass('active');
    });

    // remove/keep label on blur
    materialInputs.on('blur', function () {
        $(this).siblings('.label-material').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-material').addClass('active');
        } else {
            $(this).siblings('.label-material').removeClass('active');
        }
    });
</script>