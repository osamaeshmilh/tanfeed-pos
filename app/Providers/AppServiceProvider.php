<?php

namespace App\Providers;

use App\GeneralSetting;
use DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        if ((isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) || (isset($_SERVER['HTTPS']) && $_SERVER['SERVER_PORT'] == 443)) {
            URL::forceScheme('https');
        }
        //setting language
        if (isset($_COOKIE['language'])) {
            \App::setLocale($_COOKIE['language']);
        } else {
            \App::setLocale('ar');
        }
        //get general setting value        
        $general_setting = DB::table('general_settings')->latest()->first();
        View::share('general_setting', $general_setting);
        config(['staff_access' => $general_setting->staff_access, 'date_format' => $general_setting->date_format]);


        $alert_product = DB::table('products')->where('is_active', true)->whereColumn('alert_quantity', '>', 'qty')->count();

        // TODO change this code to get products that well expire after dynamic months
        $month = GeneralSetting::latest()->first()->month_alert;
        $alert_expiration_product = DB::select(DB::raw(" SELECT count(*) FROM products WHERE DATE_ADD( Now() , INTERVAL 2 MONTH )  >= expiration"));

        View::share(array('alert_product' => $alert_product, 'alert_expiration_product' => $alert_expiration_product));
        Schema::defaultStringLength(191);
    }
}
